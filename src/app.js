const express = require('express')
const app = express()

app.get('/', (req, res) => {
    return res.json('instancia 2')
})

app.get('/new', (req, res) => {
    return res.json('validando mudança, instancia 2')
})

app.use((req, res, next) => {
    res.status(404)
    res.json('Resource Not Found')
})

app.listen(process.env.PORT || 3000)

module.exports = app
