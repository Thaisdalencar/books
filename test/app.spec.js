var request = require('supertest')
var app = require('../src/app.js')
describe('GET /', function () {
    it('respond default route', function(done) {
        request(app).get('/').expect('"app ativo"', done)
    })
})